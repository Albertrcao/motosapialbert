import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Moto } from 'src/app/motos/motos.model';
import { DatosService } from 'src/app/service/datos.service';

@Component({
  selector: 'app-motos-info',
  templateUrl: './motos-info.component.html',
  styleUrls: ['./motos-info.component.scss'],
})

export class MotosInfoComponent implements OnInit {

  @Input() motos: Moto[] = [];

  constructor(private data: DatosService, private router: Router) { }

  ngOnInit() {}

  detalleMoto(moto: Moto){

    this.data.setSingleMoto(moto)
    this.router.navigate([`/motos/${moto.id}`]);

  }
}
