import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MotosPage } from './motos.page';

const routes: Routes = [
  {
    path: '',
    component: MotosPage
  },
  {
    path: 'anadir-moto',
    loadChildren: () => import('./anadir-moto/anadir-moto.module').then( m => m.AnadirMotoPageModule)
  },
  {
    path: 'moto-detalle',
    loadChildren: () => import('./moto-detalle/moto-detalle.module').then( m => m.MotoDetallePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MotosPageRoutingModule {}
