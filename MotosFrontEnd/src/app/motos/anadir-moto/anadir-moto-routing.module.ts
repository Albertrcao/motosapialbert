import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnadirMotoPage } from './anadir-moto.page';

const routes: Routes = [
  {
    path: '',
    component: AnadirMotoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnadirMotoPageRoutingModule {}
