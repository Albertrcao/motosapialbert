import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatosService } from 'src/app/service/datos.service';

@Component({
  selector: 'app-anadir-moto',
  templateUrl: './anadir-moto.page.html',
  styleUrls: ['./anadir-moto.page.scss'],
})
export class AnadirMotoPage implements OnInit {

  marcaa: string = '';
  modeloo: string = '';
  year: string = '';
  precioo: string = '';
  photo: string = '';
  cameraImage: string | ArrayBuffer = '../../../assets/camera.svg';

  constructor(private data: DatosService, private router: Router) { }

  ngOnInit() {
  }

  cameraImageChange(file: File){

    this.photo = file[0];
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    reader.onload = () => {
      this.cameraImage = reader.result;
    };
  }

  anadirNuevaMoto(){

    var jsonStr = {foto: 'http://motos-b60.kxcdn.com/sites/default/files/ducati-scrambler-icon-2019.jpg' , marca: this.marcaa, modelo: this.modeloo, year: this.year, precio: this.precioo};

    const formData = new FormData;
    
    const precioString = this.precioo + '';

    /*formData.append('foto', this.photo);
    formData.append('marca', this.marcaa);
    formData.append('modelo', this.modeloo);
    formData.append('year', this.year);
    formData.append('precio', precioString);*/

    console.log(jsonStr);

    this.data.anadirMoto(jsonStr);
    this.router.navigate(['/motos']);
  
  }
}
