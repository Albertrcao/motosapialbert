import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { DatosService } from '../service/datos.service';
import { Moto, MotoFiltrada } from './motos.model';

@Component({
  selector: 'app-motos',
  templateUrl: './motos.page.html',
  styleUrls: ['./motos.page.scss'],
})

export class MotosPage implements OnInit {

  motos: Moto[] = [];
  singleMoto: Moto;
  motosFiltradas: MotoFiltrada[] = [
    {
      title: 'Ducati',
      image: 'https://www.pngfind.com/pngs/m/512-5124158_ducati-logo-graphics-logo-ducati-hd-png-download.png',
    },
    {
      title: 'Yamaha',
      image: 'https://c0.klipartz.com/pngpicture/875/92/sticker-png-yamaha-motor-company-yamaha-corporation-motorcycle-logo-motorcycle-company-logo-motorcycle-desk-yamaha.png',
    },
    {
      title: 'Honda',
      image: 'https://c0.klipartz.com/pngpicture/521/131/gratis-png-honda-logo-moto-cascos-coche-honda.png',
    },
    {
      title: 'Todas las Motos',
      image: 'https://c0.klipartz.com/pngpicture/868/182/gratis-png-honda-logo-honda-logo-moto-honda.png', 
    },
  ];

  constructor(private data: DatosService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
  
    this.cargarMotos();
    console.log('OnInit');

  }

  ionViewWillEnter(){

    this.cargarMotos();
    console.log('IonView');

  }

  cargarMotos(){

    (async () => {this.motos = await this.data.getMotos();})();
    
  }

  filtroMotos(marcaMoto: string){

    if (marcaMoto === 'Todas las Motos'){
      this.cargarMotos();
    } else {
      (async () => {this.motos = await this.data.getMotoFiltrada(marcaMoto);})();
    }

  }

  anadirNuevaMoto(){

    this.router.navigate(['/motos/anadir-moto']);

  }

  detalleMoto(moto: Moto){

    let navigationExtras: NavigationExtras = {
      state: {
        parametro: moto,
      },
    };
    this.router.navigate(['/motos/moto-detalle'], navigationExtras);

  }
  
}
