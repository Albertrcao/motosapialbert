import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MotoDetallePageRoutingModule } from './moto-detalle-routing.module';
import { MotoDetallePage } from './moto-detalle.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MotoDetallePageRoutingModule,
    ComponentsModule
  ],
  declarations: [MotoDetallePage]
})
export class MotoDetallePageModule {}
