import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MotoDetallePage } from './moto-detalle.page';

const routes: Routes = [
  {
    path: '',
    component: MotoDetallePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MotoDetallePageRoutingModule {}
