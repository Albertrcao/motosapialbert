import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { DatosService } from 'src/app/service/datos.service';
import { Moto } from '../motos.model';

@Component({
  selector: 'app-moto-detalle',
  templateUrl: './moto-detalle.page.html',
  styleUrls: ['./moto-detalle.page.scss'],
})
export class MotoDetallePage implements OnInit {

  cargarMoto: any = [];

  constructor(private alertController: AlertController, private data: DatosService, private router: Router, private route: ActivatedRoute) { 

    this.route.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state) {
        console.log();
        this.cargarMoto = this.router.getCurrentNavigation().extras.state.parametro;
        console.log(this.cargarMoto);
      }
    });

  }

  ngOnInit() {

    /*this.cargarMoto = this.data.getSingleMoto();

    if (!this.cargarMoto || this.cargarMoto === undefined){
      this.router.navigate(['/motos']);
    }

    console.log(this.cargarMoto);*/

  }

  borrarMoto(){

    this.presentAlertConfirm();

  }

  async presentAlertConfirm(){

    const alerta = await this.alertController.create({

      cssClass: 'my-custom-class',
      header: 'Cuidado!',
      message: 'Estás seguro de que deseas eliminar esta moto?',
      buttons: [
        {text: 'CANCELAR',
        role: 'cancel',
        cssClass: 'secondary',
        handler: () => {console.log('Cancelando');}
      },
      {
        text: 'VALE',
        handler: () => {this.data.borrarMoto(this.cargarMoto.id);
                        this.router.navigate(['/motos']);
      }
    }
      ]

    });

    await alerta.present();

  }

}
