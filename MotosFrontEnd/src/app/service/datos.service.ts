import { Injectable } from '@angular/core';
import { Moto } from '../motos/motos.model';

@Injectable({
  providedIn: 'root'
})

export class DatosService {

  BASE_URL: string = 'http://albert-rodriguez-7e3.alwaysdata.net/apimotos'; //mi api
  motos: Moto[] = [];
  singleMoto: Moto;

  constructor() { }

  async getMotos(){

    this.motos = [];
    this.motos = await (await fetch(`${this.BASE_URL}/getmotos`)).json();
  
    return this.motos;

  }

  setSingleMoto(motoSeleccionada: Moto){

    this.singleMoto = motoSeleccionada;

  }

  getSingleMoto(){

    return this.singleMoto;

  }

  async getMotoFiltrada(marca: string){

    this.motos = await (await fetch(`${this.BASE_URL}/FiltrarMoto?marca=${marca}`)).json();

    return this.motos;

  }

  async borrarMoto(id: number){

    const eliminarMoto = await fetch(`${this.BASE_URL}/borrarMoto/${id}`, {method: 'DELETE',});

    return eliminarMoto;

  }

  async anadirMoto(nuevaMoto){
    console.log(nuevaMoto);
    const nuevaMotoAlbert = await fetch(`${this.BASE_URL}/moto`, {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify(nuevaMoto),})
    .then(response => response.json())
    .then(data => {
    console.log('Success:', data);
  })
    .catch((error) => {
    console.error('Error:', error);
});  
  }
}
